import {Box} from './Box';
import styled from 'styled-components/native';

export const Col = styled(Box)`
  flex-grow: 1;
  flex-shrink: 1;
  flex-basis: auto;
`;

Col.displayName = 'Col';
Col.defaultProps = {
  px: 3,
  // flexGrow: 1,
  // flexShrink: 1,
  // flexBasis: 'auto',
  // flex: '1',
  // flex: '1 1 auto',
};

// export const Col: React.FC<BoxProps> = (props) => <Box flex={1} pl={1} pr={1} {...props} />
