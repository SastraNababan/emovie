import {Text} from './Text';
import styled from 'styled-components/native';

export const H2 = styled(Text)``;

H2.displayName = 'H2';
H2.defaultProps = {
  fontSize: '28px',
  lineHeight: '34px',
};
