import {Text} from './Text';
import styled from 'styled-components/native';

export const H1 = styled(Text)`
  font-size: 36px;
  line-height: 42px;
`;

H1.displayName = 'H1';
H1.defaultProps = {
  fontSize: '36px',
  color: 'blue',
  fontFamily: 'Neue Haas Grotesk Display Pro',
};
