import {Flex} from './Flex';
import styled from 'styled-components/native';

export const Row = styled(Flex)``;

Row.displayName = 'Row';
Row.defaultProps = {
  mx: -3,
  flexWrap: 'wrap',
  flexDirection: 'row',
};

// export const Row: React.FC<BoxProps> = (props) => <Box row wrap {...props} />
