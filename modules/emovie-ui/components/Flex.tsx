import {Box} from './Box';
import styled from 'styled-components/native';
export const Flex = styled(Box)``;

Flex.displayName = 'Flex';
Flex.defaultProps = {
  flex: 1,
};
