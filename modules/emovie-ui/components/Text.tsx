import {TextProps as RNTextProps} from 'react-native';
import styled from 'styled-components/native';
import {color, ColorProps, typography, TypographyProps} from 'styled-system';

import {
  basics,
  BasicsProps,
  dimensions,
  DimensionsProps,
  positions,
  PositionsProps,
  space,
  SpaceProps,

  // typography,
  // TypographyProps,
} from '../styles';
export interface TextProps
  extends BasicsProps,
    DimensionsProps,
    PositionsProps,
    ColorProps,
    SpaceProps,
    TypographyProps,
    RNTextProps {}

export const Text = styled.Text<TextProps>(
  basics,
  dimensions,
  positions,
  space,
  color,
  typography,
  // {fontFamily: 'Inter', fontSize: 14, lineHeight: '20px'},
);

Text.defaultProps = {
  fontFamily: 'Inter',
  fontSize: 14,
};
