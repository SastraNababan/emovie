import {ButtonProps as RNTextProps, PressableProps} from 'react-native';
import styled from 'styled-components/native';
import {color, ColorProps, typography, TypographyProps} from 'styled-system';

import {
  basics,
  BasicsProps,
  dimensions,
  DimensionsProps,
  positions,
  PositionsProps,
  space,
  SpaceProps,

  // typography,
  // TypographyProps,
} from '../styles';
export interface ButtonProps
  extends BasicsProps,
    DimensionsProps,
    PositionsProps,
    ColorProps,
    SpaceProps,
    TypographyProps,
    RNTextProps,
    PressableProps {}

export const Button = styled.Text<ButtonProps>(
  basics,
  dimensions,
  positions,
  space,
  color,
  typography,
  // {fontFamily: 'Inter', fontSize: 14, lineHeight: '20px'},
);

Button.defaultProps = {
  fontFamily: 'Inter',
  fontSize: 14,
};
