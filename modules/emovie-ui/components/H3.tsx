import {Text} from './Text';
import styled from 'styled-components/native';
export const H3 = styled(Text)``;

H3.displayName = 'H3';
H3.defaultProps = {
  fontSize: '20px',
  lineHeight: '29px',
};
