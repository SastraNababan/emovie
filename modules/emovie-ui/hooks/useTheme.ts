import {useEffect, useState} from 'react';
import {Theme, ThemeMode} from '../theme/types';
import {useAsyncStorage} from './useAsyncStorage';

export interface ThemeState {
  themeMode: ThemeMode;
  theme: Theme;
  hasThemeLoaded: boolean;
}

const initialThemeState = {
  themeMode: 'light',
  hasThemeLoaded: false,
} as ThemeState;

/**
 * Effect
 */
export const useTheme = () => {
  const [themeState, setThemeState] = useState<ThemeState>(initialThemeState);
  const {loadString, saveString} = useAsyncStorage('themeMode');

  useEffect(() => {
    if (!themeState.hasThemeLoaded) {
      loadString().then((themeMode: ThemeMode) => {
        if (themeMode !== null) {
          setThemeState({
            ...themeState,
            themeMode,
            hasThemeLoaded: true,
          });
        }
      });
    }
  });

  const toggle = () => {
    const themeMode = themeState.themeMode === 'light' ? 'dark' : 'light';
    // const {saveString} = useAsyncStorage('themeMode');

    saveString(themeMode).then(() =>
      // eslint-disable-next-line no-console
      console.log(`Cached Theme -> Saved: ${themeMode}`),
    );

    setThemeState({
      ...themeState,
      themeMode,
    });
  };

  const {themeMode} = themeState;

  return {themeMode, toggle};
};
