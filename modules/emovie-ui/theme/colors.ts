// color palette : https://www.themes.dev/tailwindcss-colors/
import {colorSystem} from './colorSystem';

export const colors = {
  black: '#000',
  white: '#fff',
};
Object.entries(colorSystem).map(color => {
  // console.log(color);
  Object.entries(color[1]).map(subColor => {
    colors[color[0] + '-' + subColor[0]] = subColor[1];
  });
});

// TODO : add dark colors
export const darkColors = {
  ...colors,
};

export const gradients = {
  default: ['#1DE9B6', '#1DC4E9'],
  secondary: ['#E5E7E8', '#AFB2B4'],
  orange: ['#f97f63', '#e54664'],
  blue: ['#4b92ff', '#1a2980'],
  pink: ['#ff2366', '#8d4de8'],
  purple: ['#7b1fa2', '#4527a0'],
  green: ['#38ef7d', '#11998e'],
};
