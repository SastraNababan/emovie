import {
  color,
  compose,
  fontSize,
  FontSizeProps,
  fontWeight,
  FontWeightProps,
  letterSpacing,
  LetterSpacingProps,
  lineHeight,
  LineHeightProps,
  textAlign,
  TextAlignProps,
} from 'styled-system';
import {defaultTheme, FontFamily, FontStyle} from '../theme';

export interface FontsStyleProps extends FontSizeProps {
  font?: FontFamily;
  fontStyle?: FontStyle;
  // fontWeight?: FontWeight;
}

export interface TypographyProps
  extends FontSizeProps,
    LetterSpacingProps,
    LineHeightProps,
    TextAlignProps,
    FontWeightProps,
    FontsStyleProps {
  muted?: boolean;
}
export const typography = compose(
  color,
  fontSize,
  letterSpacing,
  lineHeight,
  textAlign,
  fontWeight,
  ({
    theme = defaultTheme,
    // font = 'regular',
    // fontStyle = 'normal',
    // fontWeight = 'normal',
  }) => ({
    fontFamily: theme.fonts.regular.normal.normal,
  }),
  props => props.muted && {opacity: 0.8},
);
