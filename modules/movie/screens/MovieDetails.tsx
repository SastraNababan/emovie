import {Box, Image, Screen, Text} from '@emovie-ui';
import MovieList from '@modules/shared/components/MovieList';
import PersonList from '@modules/shared/components/PersonList';
import {MovieDetails} from '@modules/shared/types/tmdb';
import {getImageUrl} from '@modules/shared/utils';
import {useRoute} from '@react-navigation/native';
import React from 'react';
import {useWindowDimensions} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {createShimmerPlaceholder} from 'react-native-shimmer-placeholder';
import SynopsisShimmer from '@modules/shared/components/SynopsisShimmer';
import useQueryMovieCredit from '../hooks/useQueryMovieCredit';
import useQueryMovieDetail from '../hooks/useQueryMovieDetail';
import useQueryMovieRecommendation from '../hooks/useQueryMovieRecommendation';

const ASPECT_RATIO = 16 / 9;

const ShimmerPlaceHolder = createShimmerPlaceholder(LinearGradient);

const Poster = ({
  data,
  isLoading,
}: {
  data: MovieDetails | undefined;
  isLoading: boolean;
}) => {
  // const {title, backdrop_path} = data;
  const {width} = useWindowDimensions();
  const imageWidth = width;
  const imageHeight = width / ASPECT_RATIO;

  const urlImage = {
    uri: getImageUrl({
      size: 'w780',
      path: data?.backdrop_path || '',
    }),
  };

  return isLoading ? (
    <ShimmerPlaceHolder
      LinearGradient={LinearGradient}
      width={width}
      height={imageHeight}
    />
  ) : (
    <Box>
      <Image
        source={urlImage}
        style={{width: imageWidth, height: imageHeight}}
        resizeMode="contain"
      />
      <Box
        position="absolute"
        bg="rgba(0, 55, 251, 0.25)"
        width="100%"
        height={30}
        bottom={0}
        alignItems="center"
        justifyContent="center">
        <Text fontSize={2} fontWeight="bold" color="white">
          {data?.title}
        </Text>
      </Box>
    </Box>
  );
};

const MiniInfo = ({
  isLoading,
  title,
  content,
}: {
  isLoading: boolean;
  title: string;
  content?: string | Date | number;
}) => {
  return isLoading ? (
    <Box>
      <ShimmerPlaceHolder
        LinearGradient={LinearGradient}
        width={100}
        height={10}
        style={{marginBottom: 5}}
      />
      <ShimmerPlaceHolder
        LinearGradient={LinearGradient}
        width={100}
        height={10}
      />
    </Box>
  ) : (
    <Box>
      <Text fontSize={0} color="gray-500">
        {title}
      </Text>
      <Text>{content}</Text>
    </Box>
  );
};

const MovieDetailsScreen = () => {
  const route = useRoute();
  const {id} = route.params;
  const {data: dataDetail, isLoading} = useQueryMovieDetail(id);
  const dataCredit = useQueryMovieCredit(id);
  const dataRecommendation = useQueryMovieRecommendation(id);
  return (
    <Screen scrollable={true}>
      <Poster data={dataDetail} isLoading={isLoading} />

      {/* Mini Info */}
      <Box flexDirection="row" justifyContent="space-around" py={2}>
        <MiniInfo
          isLoading={isLoading}
          title="Release Date"
          content={dataDetail?.release_date}
        />

        <MiniInfo
          isLoading={isLoading}
          title="Duration"
          content={dataDetail?.runtime}
        />
        <MiniInfo
          isLoading={isLoading}
          title="Rating"
          content={dataDetail?.vote_average}
        />
      </Box>

      {/* Synopsis */}
      <Box mt={2} p={2}>
        {isLoading ? (
          <SynopsisShimmer isLoading={true} />
        ) : (
          <>
            <Text fontWeight="bold" fontSize={2} pb={2}>
              Synopsis
            </Text>

            {dataDetail?.overview && (
              <Text numberOfLines={4}>{dataDetail.overview}</Text>
            )}
          </>
        )}
      </Box>

      {/* Main Cast */}
      <Box mt={2} p={2}>
        {!dataCredit.isLoading && (
          <>
            <Text fontWeight="bold" fontSize={2} pb={2}>
              Main Cast
            </Text>
            <PersonList
              data={dataCredit?.data?.cast}
              isLoading={dataCredit.isLoading}
            />
          </>
        )}
      </Box>

      {/* Related */}
      <Box mt={2} p={2}>
        {!dataRecommendation.isLoading && (
          <>
            <Text fontWeight="bold" fontSize={2} pb={2}>
              Related
            </Text>
            <MovieList
              data={dataRecommendation?.data?.results || []}
              isLandscape={false}
              isLoading={dataRecommendation.isLoading}
            />
          </>
        )}
      </Box>
    </Screen>
  );
};

export default MovieDetailsScreen;
