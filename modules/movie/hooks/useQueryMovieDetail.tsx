import request from '@modules/shared/libs/request';
import {MovieDetails} from '@modules/shared/types/tmdb';
import {useQuery} from 'react-query';

const _fetch = async id => {
  const url = `movie/${id}`;

  try {
    const response = await request(url);
    return response;
  } catch (error) {
    throw new Error(error.message);
  }
};

const useQueryMovieDetail = id => {
  return useQuery<MovieDetails>(['movie-detail', id], () => _fetch(id));
};

export default useQueryMovieDetail;
