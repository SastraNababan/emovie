import request from '@modules/shared/libs/request';
import {useQuery} from 'react-query';

const _fetch = async id => {
  const url = `movie/${id}/credits`;

  try {
    const response = await request(url);
    return response;
  } catch (error) {
    throw new Error(error.message);
  }
};

const useQueryMovieCredit = id => {
  return useQuery(['movie-credit', id], () => _fetch(id));
};

export default useQueryMovieCredit;
