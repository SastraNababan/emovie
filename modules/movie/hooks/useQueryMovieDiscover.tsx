import request from '@modules/shared/libs/request';
import {Movie} from '@modules/shared/types/tmdb';
import {useQuery} from 'react-query';

const _fetch = async () => {
  const url = 'discover/movie';

  try {
    const response = await request(url);
    return response;
  } catch (error) {
    throw new Error(error.message);
  }
};
type MoveDetailProps = {
  page: number;
  results: Movie[];
  total_pages: number;
  total_results: number;
};
const useQueryMovieDiscover = () => {
  return useQuery<MoveDetailProps>(['movie-discover'], _fetch);
};

export default useQueryMovieDiscover;
