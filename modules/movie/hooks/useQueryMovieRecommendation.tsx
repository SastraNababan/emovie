import request from '@modules/shared/libs/request';
import {Movie} from '@modules/shared/types/tmdb';
import {useQuery} from 'react-query';

const _fetch = async id => {
  const url = `movie/${id}/recommendations`;

  try {
    const response = await request(url);
    return response;
  } catch (error) {
    throw new Error(error.message);
  }
};
type MoveDetailProps = {
  page: number;
  results: Movie[];
  total_pages: number;
  total_results: number;
};
const useQueryMovieRecommendation = id => {
  return useQuery<MoveDetailProps>(['movie-recommendation', id], () =>
    _fetch(id),
  );
};

export default useQueryMovieRecommendation;
