import request from '@modules/shared/libs/request';
import {TvShow} from '@modules/shared/types/tmdb';
import {useQuery} from 'react-query';

const _fetch = async () => {
  const url = 'discover/tv';

  try {
    const response = await request(url);
    return response;
  } catch (error) {
    throw new Error(error.message);
  }
};
type Props = {
  page: number;
  results: TvShow[];
  total_pages: number;
  total_results: number;
};
const useQueryTvDiscover = () => {
  return useQuery<Props>(['tv-discover'], _fetch);
};

export default useQueryTvDiscover;
