import request from '@modules/shared/libs/request';
import {TvShowDetails} from '@modules/shared/types/tmdb';
import {useQuery} from 'react-query';

const _fetch = async id => {
  const url = `tv/${id}`;

  try {
    const response = await request(url);
    return response;
  } catch (error) {
    throw new Error(error.message);
  }
};

const useQueryTvDetail = id => {
  return useQuery<TvShowDetails>(['movie-detail', id], () => _fetch(id));
};

export default useQueryTvDetail;
