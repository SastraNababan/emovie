import request from '@modules/shared/libs/request';
import {useQuery} from 'react-query';

const _fetch = async id => {
  const url = `tv/${id}/credits`;

  try {
    const response = await request(url);
    return response;
  } catch (error) {
    throw new Error(error.message);
  }
};

const useQueryTVCredit = id => {
  return useQuery(['tv-credit', id], () => _fetch(id));
};

export default useQueryTVCredit;
