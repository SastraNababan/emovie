import request from '@modules/shared/libs/request';
import {TvShow} from '@modules/shared/types/tmdb';
import {useQuery} from 'react-query';

const _fetch = async id => {
  const url = `tv/${id}/recommendations`;

  try {
    const response = await request(url);
    return response;
  } catch (error) {
    throw new Error(error.message);
  }
};
type Props = {
  page: number;
  results: TvShow[];
  total_pages: number;
  total_results: number;
};
const useQueryTvRecommendation = id => {
  return useQuery<Props>(['movie-recommendation', id], () => _fetch(id));
};

export default useQueryTvRecommendation;
