import {Box, Image, Screen} from '@emovie-ui';
import {getImageUrl} from '@modules/shared/utils';
import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {Pressable, useWindowDimensions} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import useQueryTvDiscover from '../hooks/useQueryTvDiscover';
const TVScreen = () => {
  const ASPECT_RATIO = 9 / 16;
  const {width} = useWindowDimensions();
  const {data} = useQueryTvDiscover();
  const navigation = useNavigation();
  return (
    <Screen>
      <FlatList
        data={data?.results || []}
        numColumns={3}
        renderItem={({item}) => {
          const imageUrl = getImageUrl({
            size: 'w780',
            path: item.poster_path,
          });
          const imgWidth = width / 3 - 20;
          const imgHeight = imgWidth / ASPECT_RATIO;
          return (
            <Box
              mb={1}
              style={{
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 3.84,

                elevation: 5,
              }}>
              <Pressable
                onPress={() =>
                  navigation.navigate('TV Show', {
                    screen: 'TvShowDetails',
                    params: item,
                  })
                }>
                <Image
                  source={{
                    uri: imageUrl,
                  }}
                  style={{
                    width: imgWidth,
                    height: imgHeight,
                    margin: 10,
                    borderRadius: 4,
                  }}
                />
              </Pressable>
            </Box>
          );
        }}
        onEndReached={() => {
          // console.log('on end reached');
        }}
      />
    </Screen>
  );
};

export default TVScreen;
