import {Box, Image, Screen, Text} from '@emovie-ui';
import PersonList from '@modules/shared/components/PersonList';
import SynopsisShimmer from '@modules/shared/components/SynopsisShimmer';
import TvShowList from '@modules/shared/components/TvShowList';
import {TvShowDetails} from '@modules/shared/types/tmdb';
import {getImageUrl} from '@modules/shared/utils';
import {useRoute} from '@react-navigation/native';
import React from 'react';
import {useWindowDimensions} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {createShimmerPlaceholder} from 'react-native-shimmer-placeholder';
import useQueryTVCredit from '../hooks/useQueryTvCredit';
import useQueryTvDetail from '../hooks/useQueryTvDetail';
import useQueryTvRecommendation from '../hooks/useQueryTvRecommendation';
const ASPECT_RATIO = 16 / 9;

const ShimmerPlaceHolder = createShimmerPlaceholder(LinearGradient);

const Poster = ({
  data,
  isLoading,
}: {
  data: TvShowDetails | undefined;
  isLoading: boolean;
}) => {
  // const {title, backdrop_path} = data;
  const {width} = useWindowDimensions();
  const imageWidth = width;
  const imageHeight = width / ASPECT_RATIO;

  const urlImage = {
    uri: getImageUrl({
      size: 'w780',
      path: data?.backdrop_path || '',
    }),
  };

  return isLoading ? (
    <ShimmerPlaceHolder
      LinearGradient={LinearGradient}
      width={width}
      height={imageHeight}
    />
  ) : (
    <Box>
      <Image
        source={urlImage}
        style={{width: imageWidth, height: imageHeight}}
        resizeMode="contain"
      />
      <Box
        position="absolute"
        bg="rgba(0, 55, 251, 0.25)"
        width="100%"
        height={30}
        bottom={0}
        alignItems="center"
        justifyContent="center">
        <Text fontSize={2} fontWeight="bold" color="white">
          {data?.name}
        </Text>
      </Box>
    </Box>
  );
};

const MiniInfo = ({
  isLoading,
  title,
  content,
}: {
  isLoading: boolean;
  title: string;
  content?: string | Date | number;
}) => {
  return isLoading ? (
    <Box>
      <ShimmerPlaceHolder
        LinearGradient={LinearGradient}
        width={100}
        height={10}
        style={{marginBottom: 5}}
      />
      <ShimmerPlaceHolder
        LinearGradient={LinearGradient}
        width={100}
        height={10}
      />
    </Box>
  ) : (
    <Box>
      <Text fontSize={0} color="gray-500">
        {title}
      </Text>
      <Text>{content}</Text>
    </Box>
  );
};

const TvDetailScreen = () => {
  const route = useRoute();
  const {id} = route.params;
  const {data: dataDetail, isLoading} = useQueryTvDetail(id);
  const dataCredit = useQueryTVCredit(id);
  const dataRecommendation = useQueryTvRecommendation(id);
  return (
    <Screen scrollable={true}>
      <Poster data={dataDetail} isLoading={isLoading} />

      {/* Mini Info */}
      <Box flexDirection="row" justifyContent="space-around" py={2}>
        <MiniInfo
          isLoading={isLoading}
          title="Last Air Date"
          content={dataDetail?.last_air_date}
        />

        <MiniInfo
          isLoading={isLoading}
          title="Episode"
          content={dataDetail?.episode_run_time}
        />
        <MiniInfo
          isLoading={isLoading}
          title="Rating"
          content={dataDetail?.vote_average}
        />
      </Box>

      {/* Synopsis */}
      <Box mt={2} p={2}>
        {isLoading ? (
          <SynopsisShimmer isLoading={true} />
        ) : (
          <>
            <Text fontWeight="bold" fontSize={2} pb={2}>
              Synopsis
            </Text>

            {dataDetail?.overview && (
              <Text numberOfLines={4}>{dataDetail.overview}</Text>
            )}
          </>
        )}
      </Box>

      {/* Main Cast */}
      <Box mt={2} p={2}>
        {!dataCredit.isLoading && (
          <>
            <Text fontWeight="bold" fontSize={2} pb={2}>
              Main Cast
            </Text>
            <PersonList
              data={dataCredit?.data?.cast}
              isLoading={dataCredit.isLoading}
            />
          </>
        )}
      </Box>

      {/* Related */}
      <Box mt={2} p={2}>
        {!dataRecommendation.isLoading && (
          <>
            <Text fontWeight="bold" fontSize={2} pb={2}>
              Related
            </Text>
            <TvShowList
              data={dataRecommendation?.data?.results || []}
              isLandscape={false}
              isLoading={dataRecommendation.isLoading}
            />
          </>
        )}
      </Box>
    </Screen>
  );
};

export default TvDetailScreen;
