import request from '@modules/shared/libs/request';
import {TvShow} from '@modules/shared/types/tmdb';
import {useQuery} from 'react-query';

const _fetch = async () => {
  const url = 'tv/popular';

  try {
    const response = await request(url);
    return response;
  } catch (error) {
    throw new Error(error.message);
  }
};
type Props = {
  page: number;
  results: TvShow[];
  total_pages: number;
  total_results: number;
};
const useQueryTvPopular = () => {
  return useQuery<Props>(['tv-popular'], _fetch);
};

export default useQueryTvPopular;
