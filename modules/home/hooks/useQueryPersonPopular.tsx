import request from '@modules/shared/libs/request';
import {Person} from '@modules/shared/types/tmdb';
import {useQuery} from 'react-query';

const _fetch = async () => {
  const url = 'person/popular';

  try {
    const response = await request(url);
    return response;
  } catch (error) {
    throw new Error(error.message);
  }
};
type Props = {
  page: number;
  results: Person[];
  total_pages: number;
  total_results: number;
};
const useQueryPersonPopular = () => {
  return useQuery<Props>(['people-popular'], _fetch);
};

export default useQueryPersonPopular;
