import request from '@modules/shared/libs/request';
import {Movie} from '@modules/shared/types/tmdb';
import {useQuery} from 'react-query';

const _fetch = async () => {
  const url = '/trending/movie/week';

  try {
    const response = await request(url);
    return response;
  } catch (error) {
    throw new Error(error.message);
  }
};
type Props = {
  page: number;
  results: Movie[];
  total_pages: number;
  total_results: number;
};
const useQueryMovieTrending = () => {
  return useQuery<Props>(['movie-trending'], _fetch);
};

export default useQueryMovieTrending;
