import request from '@modules/shared/libs/request';
import {Movie} from '@modules/shared/types/tmdb';
import {useQuery} from 'react-query';

const _fetch = async () => {
  const url = 'movie/top_rated';

  try {
    const response = await request(url);
    return response;
  } catch (error) {
    throw new Error(error.message);
  }
};
type Props = {
  page: number;
  results: Movie[];
  total_pages: number;
  total_results: number;
};
const useQueryMovieTopRated = () => {
  return useQuery<Props>(['movie-top-rated'], _fetch);
};

export default useQueryMovieTopRated;
