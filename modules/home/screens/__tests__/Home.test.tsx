import {
  cleanup,
  renderScreen,
  waitFor,
} from '@modules/shared/utils/tests/utils';
import React from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import HomeScreen from '../Home';

jest.mock('react-native-linear-gradient', () => ({
  LinearGradient: () => <></>,
}));

jest.mock('react-native-shimmer-placeholder', () => ({
  createShimmerPlaceholder: () => null,
}));

jest.mock(
  '../../../../modules/shared/components/MovieListShimmer',
  () => () => <span testID="MovieListShimmer" />,
);

jest.mock(
  '../../../../modules/shared/components/PersonListShimmer',
  () => () => <span testID="PersonListShimmer" />,
);

jest.mock('../../../../modules/home/components/TrendingCarousel', () => () => (
  <span testID="TrendingCarousel" />
));

jest.mock('@react-navigation/native');

beforeEach(() => {});

afterEach(() => {
  // jest.resetAllMocks();
  cleanup();
});

describe('Home Screen', () => {
  it('Should render All Components', async () => {
    const screen = renderScreen(
      <SafeAreaProvider
        initialMetrics={{
          frame: {x: 0, y: 0, width: 0, height: 0},
          insets: {top: 0, left: 0, right: 0, bottom: 0},
        }}>
        <HomeScreen />
      </SafeAreaProvider>,
    );
    expect(screen.getByTestId('screen')).toBeDefined();

    await waitFor(() => {
      expect(screen.getByText('Popular Movie')).toBeDefined();
      expect(screen.getByText('Top Rated Movies')).toBeDefined();
      expect(screen.getByText('Popular TV Shows')).toBeDefined();
      expect(screen.getByText('Popular People')).toBeDefined();
    });
  });
});
