import {Box, Screen} from '@emovie-ui';
import MovieList from '@modules/shared/components/MovieList';
import PersonList from '@modules/shared/components/PersonList';
import TvShowList from '@modules/shared/components/TvShowList';
import React from 'react';
import TrendingCarousel from '../components/TrendingCarousel';
import useQueryMoviePopular from '../hooks/useQueryMoviePopular';
import useQueryMovieTopRated from '../hooks/useQueryMovieTopRated';
import useQueryPersonPopular from '../hooks/useQueryPersonPopular';
import useQueryTvPopular from '../hooks/useQueryTvPopular';

const HomeScreen = () => {
  const popularMovie = useQueryMoviePopular();
  const topRatedMovie = useQueryMovieTopRated();
  const popularTv = useQueryTvPopular();
  const popularPerson = useQueryPersonPopular();
  return (
    <Screen safearea={true} scrollable={true} bg="white" testID="screen">
      <TrendingCarousel />

      <Box mt={2}>
        <MovieList
          data={popularMovie?.data?.results || []}
          isLoading={popularMovie.isLoading}
          title="Popular Movie"
          description="See which are the best films of the week"
        />
      </Box>

      <Box mt={4}>
        <MovieList
          data={topRatedMovie?.data?.results || []}
          isLoading={topRatedMovie.isLoading}
          title="Top Rated Movies"
          description="Check out the top movies as rated by IMDb users."
          isLandscape={false}
        />
      </Box>

      <Box mt={4}>
        <TvShowList
          data={popularTv?.data?.results || []}
          title="Popular TV Shows"
          isLoading={popularTv.isLoading}
        />
      </Box>

      <Box mt={4}>
        <PersonList
          data={popularPerson?.data?.results || []}
          title="Popular People"
          isLoading={popularPerson.isLoading}
        />
      </Box>
    </Screen>
  );
};

export default HomeScreen;
