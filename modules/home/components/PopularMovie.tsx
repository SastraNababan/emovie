import {Box, Image, Text} from '@emovie-ui';
import PopularMovieData from '@modules/shared/fixtures/PopularMovie';
import React, {useMemo} from 'react';
import {FlatList, useWindowDimensions} from 'react-native';

const getImageUrl = ({size = 'w500', path}) =>
  `https://image.tmdb.org/t/p/${size}/${path}`;

const PopularMovie = () => {
  const BACKDROP_ASPECT_RATIO = 16 / 9;
  const {width} = useWindowDimensions();
  const mWidth = width / 1.7;
  const backdropStyles = useMemo(
    () => ({
      width: mWidth,
      height: mWidth / BACKDROP_ASPECT_RATIO,
      // marginTop: IS_ANDROID ? insets.top + 16 : insets.top,
      borderRadius: 4,
    }),
    [BACKDROP_ASPECT_RATIO, mWidth],
  );

  const renderItem = ({item}) => {
    // const {
    // } = item;
    const urlImage = {
      uri: getImageUrl({size: 'w780', path: item.backdrop_path}),
    };

    return (
      <Box m={2}>
        <Image source={urlImage} style={backdropStyles} />
        <Box mt={2}>
          <Text fontSize={0} fontWeight="300">
            {item.title}
          </Text>
        </Box>
      </Box>
    );
  };
  return (
    <Box>
      <FlatList
        data={PopularMovieData.results}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        horizontal
        showsHorizontalScrollIndicator={false}
      />
    </Box>
  );
};

export default PopularMovie;
