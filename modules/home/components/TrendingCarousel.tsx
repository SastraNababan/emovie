import {Box, Text} from '@emovie-ui';
import {getImageUrl} from '@modules/shared/utils';
import {useNavigation} from '@react-navigation/native';
import * as React from 'react';
import {useRef, useState} from 'react';
import {
  Image,
  Pressable,
  StyleSheet,
  useWindowDimensions,
  View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {createShimmerPlaceholder} from 'react-native-shimmer-placeholder';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import useQueryMovieTrending from '../hooks/useQueryMovieTrending';

const ShimmerPlaceHolder = createShimmerPlaceholder(LinearGradient);

const TrendingCarousel = () => {
  const carouselRef = useRef();
  const flatListRef = useRef();
  const {width} = useWindowDimensions();
  const THUMB_SIZE = 80;
  const BACKDROP_ASPECT_RATIO = 16 / 9;

  const [indexSelected, setIndexSelected] = useState(0);

  // eslint-disable-next-line no-shadow
  const onSelect = indexSelected => {
    setIndexSelected(indexSelected);
    flatListRef?.current?.scrollToOffset({
      offset: indexSelected * THUMB_SIZE,
      animated: true,
    });
  };

  const {data, isLoading} = useQueryMovieTrending();
  const navigation = useNavigation();
  return isLoading ? (
    <>
      <ShimmerPlaceHolder
        LinearGradient={LinearGradient}
        width={width}
        height={width / BACKDROP_ASPECT_RATIO}
      />
      <ShimmerPlaceHolder
        LinearGradient={LinearGradient}
        width={width - 100}
        height={10}
        style={{alignSelf: 'center', marginTop: 20, marginBottom: 20}}
      />
    </>
  ) : (
    <View style={styles.container}>
      <Carousel
        ref={carouselRef}
        layout="tinder"
        data={data?.results.slice(0, 10) || []}
        sliderWidth={width}
        itemWidth={width}
        onSnapToItem={index => onSelect(index)}
        renderItem={({item, index}) => {
          const urlImage = {
            uri: getImageUrl({
              size: 'w780',
              path: item.backdrop_path,
            }),
          };
          return (
            <View>
              <Pressable
                onPress={() =>
                  navigation.navigate('Movies', {
                    screen: 'MovieDetails',
                    params: item,
                  })
                }>
                <Image
                  key={index}
                  style={{width: width, height: width / BACKDROP_ASPECT_RATIO}}
                  resizeMode="contain"
                  source={urlImage}
                />
              </Pressable>
              <Box>
                <Text
                  color="white"
                  fontWeight="bold"
                  fontSize={3}
                  p={2}
                  bg="red-500"
                  textAlign="center">
                  {item.title}
                </Text>
              </Box>
            </View>
          );
        }}
      />
      <Pagination
        inactiveDotColor="gray"
        dotColor={'red'}
        activeDotIndex={indexSelected}
        dotsLength={10}
        animatedDuration={150}
        inactiveDotScale={1}
      />
    </View>
  );
};

export default TrendingCarousel;

const styles = StyleSheet.create({
  container: {},
});
