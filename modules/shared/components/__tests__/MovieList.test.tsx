import {
  AllProvider,
  fireEvent,
  render,
} from '@modules/shared/utils/tests/utils';
import {useNavigation} from '@react-navigation/native';
import React from 'react';
import MovieList from '../MovieList';

jest.mock('react-native-linear-gradient', () => ({
  LinearGradient: () => <></>,
}));

jest.mock('react-native-shimmer-placeholder', () => ({
  createShimmerPlaceholder: () => null,
}));

jest.mock('@react-navigation/native');

describe('MovieList Component Test', () => {
  it('renders correctly', () => {
    const props = {
      data: [],
      title: 'Movie List',
      description: 'movie description',
    };
    const component = render(
      <AllProvider>
        <MovieList {...props} />
      </AllProvider>,
    );
    expect(component).toMatchSnapshot();
  });

  it('should render component', () => {
    const props = {
      data: [],
      title: 'Movie List',
      description: 'movie description',
    };
    const component = render(
      <AllProvider>
        <MovieList {...props} />
      </AllProvider>,
    );
    expect(component.getByText('Movie List')).toBeDefined();
    expect(component.getByText('movie description')).toBeDefined();
  });

  it('should open detail', () => {
    (useNavigation as jest.MockedFunction<any>).mockReturnValue({
      navigate: () => jest.fn(),
    });

    const props = {
      data: [],
      title: 'Movie List',
      description: 'movie description',
    };
    const component = render(
      <AllProvider>
        <MovieList {...props} />
      </AllProvider>,
    );
    fireEvent.press(component.getByTestId('press'));
    expect(useNavigation).toHaveBeenCalled();
  });
});
