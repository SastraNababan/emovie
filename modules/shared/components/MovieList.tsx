import {Box, Col, Image, Row, Text} from '@emovie-ui';
import MovieListShimmer from '@modules/shared/components/MovieListShimmer';
import {useNavigation} from '@react-navigation/native';
import React, {FunctionComponent, useCallback, useMemo} from 'react';
import {FlatList, Pressable, useWindowDimensions} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import {Movie} from '../types/tmdb';
const getImageUrl = ({size = 'w500', path}) =>
  `https://image.tmdb.org/t/p/${size}/${path}`;

type MovieListProps = {
  data: Movie[];
  title?: string;
  description?: string;
  isLandscape?: boolean;
  isLoading?: boolean;
};
const MovieList: FunctionComponent<MovieListProps> = ({
  data,
  title,
  description,
  isLandscape = true,
  isLoading,
}) => {
  const navigation = useNavigation();

  const BACKDROP_ASPECT_RATIO = isLandscape ? 16 / 9 : 9 / 16;
  const {width} = useWindowDimensions();
  const mWidth = isLandscape ? width / 1.7 : width / 3;
  const backdropStyles = useMemo(
    () => ({
      width: mWidth,
      height: mWidth / BACKDROP_ASPECT_RATIO - (isLandscape ? 0 : 40),
      borderRadius: 4,
    }),
    [BACKDROP_ASPECT_RATIO, isLandscape, mWidth],
  );

  const renderItem = useCallback(
    ({item}: {item: Movie}) => {
      const {
        backdrop_path: backdropPath,
        poster_path: posterPath,
        // eslint-disable-next-line no-shadow
        title,
        release_date,
      } = item;
      const urlImage = {
        uri: getImageUrl({
          size: 'w780',
          path: isLandscape ? posterPath : backdropPath,
        }),
      };

      return (
        <Box m={2}>
          <Pressable
            onPress={() =>
              navigation.navigate('Movies', {
                screen: 'MovieDetails',
                params: item,
              })
            }>
            <Image source={urlImage} style={backdropStyles} />
          </Pressable>
          <Box mt={2}>
            {isLandscape ? (
              <Text fontSize={0} pb={1} fontWeight="300">
                {title}
              </Text>
            ) : (
              <Text
                fontSize={0}
                pb={1}
                fontWeight="300"
                numberOfLines={1}
                ellipsizeMode="tail"
                width={130}>
                {title}
              </Text>
            )}
            <Text fontSize="10px" color="gray-500">
              {release_date}
            </Text>
          </Box>
        </Box>
      );
    },
    [isLandscape, backdropStyles, navigation],
  );

  return isLoading ? (
    <MovieListShimmer isLoading={isLoading} isLandscape={isLandscape} />
  ) : (
    <Box>
      {title && (
        <Row mb={1} alignItems="center">
          <Col>
            <Text fontWeight="bold" px={2} fontSize={2}>
              {title}
            </Text>
            <Text px={2} fontSize={0}>
              {description}
            </Text>
          </Col>
          <Col alignItems="flex-end">
            <Pressable
              testID="press"
              onPress={() => {
                navigation.navigate('Movies');
              }}>
              <Box mr={2}>
                <Feather name="chevron-right" size={18} />
              </Box>
            </Pressable>
          </Col>
        </Row>
      )}
      <FlatList
        data={data}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        horizontal
        showsHorizontalScrollIndicator={false}
      />
    </Box>
  );
};

export default MovieList;
