import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {createShimmerPlaceholder} from 'react-native-shimmer-placeholder';

import {useWindowDimensions} from 'react-native';
import {Box} from '@emovie-ui';

const ShimmerPlaceHolder = createShimmerPlaceholder(LinearGradient);

const MovieListShimmer = ({
  isLoading,
  isLandscape,
}: {
  isLoading: boolean;
  isLandscape: boolean;
}) => {
  const BACKDROP_ASPECT_RATIO = isLandscape ? 16 / 9 : 9 / 16;
  const {width} = useWindowDimensions();
  const mWidth = isLandscape ? width / 1.7 : width / 3;
  const mHeight = mWidth / BACKDROP_ASPECT_RATIO - (isLandscape ? 0 : 40);
  return isLoading ? (
    <>
      <ShimmerPlaceHolder
        LinearGradient={LinearGradient}
        style={{marginBottom: 15}}
        width={width / 4}
        height={10}
        visible={!isLoading}
      />
      <Box flexDirection="row">
        <ShimmerPlaceHolder
          LinearGradient={LinearGradient}
          style={{marginBottom: 5, marginRight: 20}}
          width={mWidth}
          height={mHeight}
          visible={!isLoading}
        />
        <ShimmerPlaceHolder
          LinearGradient={LinearGradient}
          style={{marginBottom: 5, marginRight: 20}}
          width={mWidth}
          height={mHeight}
          visible={!isLoading}
        />
        <ShimmerPlaceHolder
          LinearGradient={LinearGradient}
          style={{marginBottom: 5, marginRight: 20}}
          width={mWidth}
          height={mHeight}
          visible={!isLoading}
        />
      </Box>
    </>
  ) : null;
};

export default MovieListShimmer;
