import {Box, Col, Image, Row, Text} from '@emovie-ui';
import {useNavigation} from '@react-navigation/native';
import React, {FunctionComponent, useMemo} from 'react';
import {FlatList, Pressable, useWindowDimensions} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import {TvShow} from '../types/tmdb';
import MovieListShimmer from './MovieListShimmer';

const getImageUrl = ({size = 'w500', path}) =>
  `https://image.tmdb.org/t/p/${size}/${path}`;

type TvShowListProps = {
  data: TvShow[];
  title?: string;
  isLandscape?: boolean;
  isLoading?: boolean;
};
const TvShowList: FunctionComponent<TvShowListProps> = ({
  data,
  title,
  isLoading,
  isLandscape = true,
}) => {
  const BACKDROP_ASPECT_RATIO = 16 / 9;
  const {width} = useWindowDimensions();
  // const insets = useSafeAreaInsets();
  const mWidth = width / 1.7;
  const backdropStyles = useMemo(
    () => ({
      width: mWidth,
      height: mWidth / BACKDROP_ASPECT_RATIO,
      // marginTop: IS_ANDROID ? insets.top + 16 : insets.top,
      borderRadius: 4,
    }),
    [mWidth, BACKDROP_ASPECT_RATIO],
  );
  const navigation = useNavigation();
  const renderItem = ({item}: {item: TvShow}) => {
    // eslint-disable-next-line no-shadow
    const {backdrop_path: backdropPath, name: title, first_air_date} = item;
    const urlImage = {
      uri: getImageUrl({size: 'w780', path: backdropPath}),
    };

    return (
      <Box m={2}>
        <Pressable
          onPress={() =>
            navigation.navigate('TV Show', {
              screen: 'TvShowDetails',
              params: item,
            })
          }>
          <Image source={urlImage} style={backdropStyles} />
        </Pressable>
        <Box mt={2}>
          <Text fontSize={0} pb={1} fontWeight="300">
            {title}
          </Text>
          <Text fontSize="10px" color="gray-500">
            {first_air_date}
          </Text>
        </Box>
      </Box>
    );
  };
  return isLoading ? (
    <MovieListShimmer isLoading={isLoading} isLandscape={isLandscape} />
  ) : (
    <Box>
      {title && (
        <Row mb={1}>
          <Col>
            <Text fontWeight="bold" px={2} fontSize={2}>
              {title}
            </Text>
          </Col>
          <Col alignItems="flex-end">
            <Pressable
              onPress={() => {
                navigation.navigate('TvShow');
              }}>
              <Box mr={2}>
                <Feather name="chevron-right" size={15} />
              </Box>
            </Pressable>
          </Col>
        </Row>
      )}
      <FlatList
        data={data}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        horizontal
        showsHorizontalScrollIndicator={false}
      />
    </Box>
  );
};

export default TvShowList;
