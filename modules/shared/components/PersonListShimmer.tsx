import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {createShimmerPlaceholder} from 'react-native-shimmer-placeholder';

import {useWindowDimensions} from 'react-native';
import {Box} from '@emovie-ui';

const ShimmerPlaceHolder = createShimmerPlaceholder(LinearGradient);

const PersonListShimmer = ({isLoading}: {isLoading: boolean}) => {
  const {width} = useWindowDimensions();
  return isLoading ? (
    <>
      <ShimmerPlaceHolder
        LinearGradient={LinearGradient}
        style={{marginBottom: 15}}
        width={width / 4}
        height={10}
        visible={!isLoading}
      />
      <Box flexDirection="row">
        <ShimmerPlaceHolder
          LinearGradient={LinearGradient}
          style={{marginBottom: 5, marginRight: 20, borderRadius: 100}}
          width={60}
          height={60}
          visible={!isLoading}
        />
        <ShimmerPlaceHolder
          LinearGradient={LinearGradient}
          style={{marginBottom: 5, marginRight: 20, borderRadius: 100}}
          width={60}
          height={60}
          visible={!isLoading}
        />
        <ShimmerPlaceHolder
          LinearGradient={LinearGradient}
          style={{marginBottom: 5, marginRight: 20, borderRadius: 100}}
          width={60}
          height={60}
          visible={!isLoading}
        />
        <ShimmerPlaceHolder
          LinearGradient={LinearGradient}
          style={{marginBottom: 5, marginRight: 20, borderRadius: 100}}
          width={60}
          height={60}
          visible={!isLoading}
        />
        <ShimmerPlaceHolder
          LinearGradient={LinearGradient}
          style={{marginBottom: 5, marginRight: 20, borderRadius: 100}}
          width={60}
          height={60}
          visible={!isLoading}
        />
      </Box>
    </>
  ) : null;
};

export default PersonListShimmer;
