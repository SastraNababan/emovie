import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {createShimmerPlaceholder} from 'react-native-shimmer-placeholder';

import {useWindowDimensions} from 'react-native';

const ShimmerPlaceHolder = createShimmerPlaceholder(LinearGradient);

const SynopsisShimmer = ({isLoading}: {isLoading: boolean}) => {
  const {width} = useWindowDimensions();
  return isLoading ? (
    <>
      <ShimmerPlaceHolder
        LinearGradient={LinearGradient}
        style={{marginBottom: 15}}
        width={width / 4}
        height={10}
        visible={!isLoading}
      />
      <ShimmerPlaceHolder
        LinearGradient={LinearGradient}
        style={{marginBottom: 5}}
        width={width}
        height={10}
        visible={!isLoading}
      />
      <ShimmerPlaceHolder
        LinearGradient={LinearGradient}
        style={{marginBottom: 5}}
        width={width}
        height={10}
        visible={!isLoading}
      />
      <ShimmerPlaceHolder
        LinearGradient={LinearGradient}
        style={{marginBottom: 5}}
        width={width}
        height={10}
        visible={!isLoading}
      />
    </>
  ) : null;
};

export default SynopsisShimmer;
