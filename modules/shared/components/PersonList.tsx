import {Box, Col, Image, Row, Text} from '@emovie-ui';
import React, {FunctionComponent, useMemo} from 'react';
import {FlatList} from 'react-native';
import {Person} from '../types/tmdb';
import PersonListShimmer from './PersonListShimmer';

const getImageUrl = ({size = 'w500', path}) =>
  `https://image.tmdb.org/t/p/${size}/${path}`;

type PersonListProps = {
  data: Person[];
  title?: string;
  isLoading?: boolean;
};
const PersonList: FunctionComponent<PersonListProps> = ({
  data,
  title,
  isLoading,
}) => {
  const backdropStyles = useMemo(
    () => ({
      width: 60,
      height: 60,
      borderRadius: 100,
      marginRight: 20,
    }),
    [],
  );

  const renderItem = ({item}: {item: Person}) => {
    const {profile_path, name} = item;
    const urlImage = {
      uri: getImageUrl({size: 'w300', path: profile_path}),
    };

    return (
      <Box m={2} borderRadius={4} alignItems="center">
        <Image
          source={urlImage}
          style={backdropStyles}
          height={60}
          width={60}
        />
        <Box mt={2}>
          <Text fontSize={0} pb={1} fontWeight="600">
            {name}
          </Text>
        </Box>
      </Box>
    );
  };
  return isLoading ? (
    <PersonListShimmer isLoading={isLoading} />
  ) : (
    <Box>
      {title && (
        <Row mb={1}>
          <Col>
            <Text fontWeight="bold" px={2} fontSize={2}>
              {title}
            </Text>
          </Col>
          <Col alignItems="flex-end">
            {/* <Pressable onPress={() => console.log('press')}>
              <Box mr={2}>
                <Feather name="chevron-right" size={15} />
              </Box>
            </Pressable> */}
          </Col>
        </Row>
      )}
      <FlatList
        data={data}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        horizontal
        // showsHorizontalScrollIndicator={false}
      />
    </Box>
  );
};

export default PersonList;
