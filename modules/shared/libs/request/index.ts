import {API_URL, API_KEY} from '@modules/shared/constant';
import axios from 'axios';

const defaultContent = {
  api_key: API_KEY,
  language: 'en-US',
};

function queryString(obj) {
  return Object.entries(obj)
    .map(([index, val]) => `${index}=${val}`)
    .join('&');
}

export default async function request(url, content = {}) {
  const obj = {...defaultContent, ...content};

  // const response = await fetch(`${API_URL}/${url}?${queryString(obj)}`);
  // const data = await (debug ? response.status : response.json());
  const response = await axios.get(`${API_URL}/${url}?${queryString(obj)}`);
  return response.data;
  // const data = await (debug ? response.status : response.json());

  // return data;
}
