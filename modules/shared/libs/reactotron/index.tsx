// import Reactotron from 'reactotron-react-native';
// import { NativeModules } from 'react-native';
// import url from 'url';

// const { hostname } = url.parse(NativeModules.SourceCode.scriptURL);

// Reactotron.configure({
//   host: hostname || '',
// }) // controls connection & communication settings
//   .useReactNative({
//     networking: {
//       // optionally, you can turn it off with false.
//       ignoreUrls: /(symbolicate)|(generate_204)/,
//     },
//   }) // add all built-in react native plugins
//   .connect(); // let's connect!

// const yeOldeConsoleLog = console.log;

// // make a new one
// console.log = (...args: any[]) => {
//   // always call the old one, because React Native does magic swizzling too
//   yeOldeConsoleLog(...args);

//   // send this off to Reactotron.
//   Reactotron.display({
//     name: 'CONSOLE.LOG',
//     value: args.length === 1 ? args[0] : args,
//     preview: JSON.stringify(args),
//   });
// };
import AsyncStorage from '@react-native-async-storage/async-storage';
import Reactotron from 'reactotron-react-native';

Reactotron.setAsyncStorageHandler(AsyncStorage) // AsyncStorage would either come from `react-native` or `@react-native-community/async-storage` depending on where you get it from
  .configure() // controls connection & communication settings
  .useReactNative() // add all built-in react native plugins
  .connect(); // let's connect!
