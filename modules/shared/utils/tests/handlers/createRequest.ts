import {rest} from 'msw';
const createRequest = (url, response, method = 'get', statusCode = '200') =>
  rest[method](url, (req, res, ctx) =>
    res(ctx.status(statusCode), ctx.json(response)),
  );

export default createRequest;
