import createRequest from './createRequest';
import PopularMovieData from '@modules/shared/fixtures/PopularMovie';
import TopRatedMovie from '@modules/shared/fixtures/TopRated';
import PopularTVData from '@modules/shared/fixtures/PopularTV';
import PopularPeopleData from '@modules/shared/fixtures/PopularPeople';

export default [
  createRequest('*/movie/popular', PopularMovieData),
  createRequest('*/movie/top_rated', TopRatedMovie),
  createRequest('*/tv/popular', PopularTVData),
  createRequest('*/person/popular', PopularPeopleData),
];
