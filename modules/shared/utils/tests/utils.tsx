import {defaultTheme, ThemeProvider} from '@emovie-ui';
import {render} from '@testing-library/react-native';
import React from 'react';
import {QueryClient, QueryClientProvider} from 'react-query';

export const renderComponent = ({component}) => {
  const {...result} = render(
    <ThemeProvider defaultTheme={defaultTheme}>{component}</ThemeProvider>,
  );
  return {...result};
};

export const AllProvider = ({children}) => {
  return <ThemeProvider defaultTheme={defaultTheme}>{children}</ThemeProvider>;
};

const createTestQueryClient = () =>
  new QueryClient({
    defaultOptions: {
      queries: {
        retry: false,
      },
    },
  });

export function renderScreen(ui) {
  const testQueryClient = createTestQueryClient();
  const {rerender, ...result} = render(
    <ThemeProvider defaultTheme={defaultTheme}>
      <QueryClientProvider client={testQueryClient}>{ui}</QueryClientProvider>
    </ThemeProvider>,
  );
  return {
    ...result,
    rerender: rerenderUi =>
      rerender(
        <QueryClientProvider client={testQueryClient}>
          {rerenderUi}
        </QueryClientProvider>,
      ),
  };
}

export function createWrapper() {
  const testQueryClient = createTestQueryClient();
  return ({children}: {children: React.ReactNode}) => (
    <QueryClientProvider client={testQueryClient}>
      {children}
    </QueryClientProvider>
  );
}

// re-export everything
export * from '@testing-library/react-native';
