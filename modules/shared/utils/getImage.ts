import {IMAGE_URL} from '../constant';

export const getImageUrl = ({
  size = 'w500',
  path,
}: {
  size: string;
  path: string;
}) => `${IMAGE_URL}/t/p/${size}/${path}`;
