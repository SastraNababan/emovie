module.exports = {
  // ...expoPreset,
  preset: 'react-native',
  transformIgnorePatterns: [
    // 'node_modules/(?!(jest-)?react-native|@react-native-community|react-clone-referenced-element|react-navigation-deprecated-tab-navigator|@react-navigation/core|@react-navigation/native|expo(nent)?|@expo(nent)?/.*|@unimodules/.*|unimodules|expo-linear-gradient)',
    // 'node_modules/(?!(jest-)?react-native|@react-native-community|react-clone-referenced-element|react-navigation-deprecated-tab-navigator|@react-navigation/core|@react-navigation/native|expo(nent)?|@expo(nent)?/.*|@unimodules/.*|unimodules|expo-linear-gradient)',
    '/node_modules/(?!(@react-native|react-native|react-native-vector-icons|react-native-snap-carousel|@react-navigation/core|@react-navigation/native)/).*/',
  ],
  testPathIgnorePatterns: ['/node_modules/', '/dist/', 'e2e'],
  testEnvironment: 'node',
  transform: {
    '^.+\\.(js|ts|tsx)$': 'babel-jest',
  },
  // setupFiles: ['react-native-gesture-handler/jestSetup.js'],
  setupFilesAfterEnv: [
    // '<rootDir>/jest/setup.ts',
    '<rootDir>/jest.setup.ts',
    // 'react-native-gesture-handler/jestSetup.ts',
    '@testing-library/jest-native/extend-expect',
  ],
  verbose: false,
  // reporters: [
  //   'default',
  //   [
  //     'jest-html-reporters',
  //     {
  //       pageTitle: 'Test Report',
  //       outputPath: './jest_report.html',
  //     },
  //   ],
  // ],
  collectCoverage: false,
  // coverageThreshold: {
  //   global: {
  //     branches: 20,
  //     functions: 30,
  //     lines: 50,
  //     statements: 50,
  //   },
  // },
  notify: true,
};
