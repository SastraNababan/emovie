// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom';
import {setupServer} from 'msw/node';
// import { handlers } from './tests/utils'
import {setLogger} from 'react-query';
import handlers from './modules/shared/utils/tests/handlers';
import mockAsyncStorage from '@react-native-async-storage/async-storage/jest/async-storage-mock';

jest.mock('@react-native-async-storage/async-storage', () => mockAsyncStorage);
// console.log('setupTest....');
export const server = setupServer(...handlers);
// Establish API mocking before all tests.
beforeAll(() => {
  // console.log('beforeAll');
  server.listen();
});
// Reset any request handlers that we may add during the tests,
// so they don't affect other tests.
afterEach(() => {
  // console.log('afterEach');
  server.resetHandlers();
});
// Clean up after the tests are finished.
afterAll(() => {
  // console.log('afterAll');
  server.close();
});

// silence react-query errors
setLogger({
  // eslint-disable-next-line no-console
  log: console.log,
  // eslint-disable-next-line no-console
  warn: console.warn,
  error: () => {},
});
