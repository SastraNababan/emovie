import React from 'react';
import {colors} from '@modules/emovie-ui/theme';
import HomeScreen from '@modules/home/screens/Home';
import MovieScreen from '@modules/movie/screens/Movie';
import MovieDetails from '@modules/movie/screens/MovieDetails';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import 'react-native-gesture-handler';
import Feather from 'react-native-vector-icons/Feather';
import TVScreen from '@modules/tv/screens/Tv';
import TvDetailScreen from '@modules/tv/screens/TvDetails';

const blue = colors['blue-500'];
const darkBlue = colors['blue-700'];
const white = colors.white;
const gray = colors['gray-400'];

const ROUTES = {
  MOVIE_LIST: 'DiscoverMovies',
  MOVIE_DETAILS: 'MovieDetails',
  TV_LIST: 'TvShow',
  TV_DETAILS: 'TvShowDetails',
};

const TABS = {
  HOME: 'Home',
  MOVIES: 'Movies',
  TV: 'TV Show',
  // PEOPLE: 'People',
};

const defaultNavigationOptions = {
  headerTintColor: darkBlue,
  headerStyle: {
    backgroundColor: white,
  },
};

const MovieStack = createStackNavigator();
const MovieStackScreen = () => (
  <MovieStack.Navigator initialRouteName={ROUTES.MOVIE_LIST}>
    <MovieStack.Screen
      name={ROUTES.MOVIE_LIST}
      component={MovieScreen}
      options={{
        title: 'Discover Movies',
      }}
    />
    <MovieStack.Screen
      name={ROUTES.MOVIE_DETAILS}
      component={MovieDetails}
      options={{
        title: 'Movie Details',
      }}
    />
  </MovieStack.Navigator>
);

const TvShowStack = createStackNavigator();
const TvShowStackScreen = () => (
  <TvShowStack.Navigator initialRouteName={ROUTES.TV_LIST}>
    <TvShowStack.Screen
      name={ROUTES.TV_LIST}
      component={TVScreen}
      options={{
        title: 'Discover TV Show',
      }}
    />
    <TvShowStack.Screen
      name={ROUTES.TV_DETAILS}
      component={TvDetailScreen}
      options={{
        title: 'TV Show Details',
      }}
    />
  </TvShowStack.Navigator>
);

const AppNavigator = () => {
  const Tab = createBottomTabNavigator();
  const TabsConfig = {
    tabBarOptions: {
      activeTintColor: blue,
      inactiveTintColor: gray,
      labelStyle: {
        margin: 0,
        padding: 1,
      },
      style: {
        backgroundColor: white,
      },
    },
  };

  return (
    <Tab.Navigator initialRouteName={ROUTES.MOVIE_LIST} {...TabsConfig}>
      <Tab.Screen
        name={TABS.HOME}
        component={HomeScreen}
        options={{
          ...defaultNavigationOptions,
          tabBarIcon: ({color}) => (
            <Feather name="home" size={20} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name={TABS.MOVIES}
        component={MovieStackScreen}
        options={{
          ...defaultNavigationOptions,
          tabBarIcon: ({color}) => (
            <Feather name="film" size={20} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name={TABS.TV}
        component={TvShowStackScreen}
        options={{
          ...defaultNavigationOptions,
          tabBarIcon: ({color}) => (
            <Feather name="tv" size={20} color={color} />
          ),
        }}
      />
      {/* <Tab.Screen
        name={TABS.PEOPLE}
        component={MovieScreen}
        options={{
          ...defaultNavigationOptions,
          tabBarIcon: ({color}) => (
            <Feather name="users" size={20} color={color} />
          ),
        }}
      /> */}
    </Tab.Navigator>
  );
};

export default AppNavigator;
