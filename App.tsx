import {defaultTheme, ThemeProvider} from '@emovie-ui';
import {NavigationContainer} from '@react-navigation/native';
import React, {useEffect} from 'react';
import 'react-native-gesture-handler';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {QueryClient, QueryClientProvider} from 'react-query';
import RNBootSplash from 'react-native-bootsplash';

import AppNavigator from './AppNavigator';
if (__DEV__) {
  import('./modules/shared/libs/reactotron').then(() =>
    // eslint-disable-next-line no-console
    console.log('Reactotron Configured'),
  );
}
const queryClient = new QueryClient();
const App = () => {
  useEffect(() => {
    const init = async () => {
      // …do multiple sync or async tasks
    };

    init().finally(async () => {
      await RNBootSplash.hide({fade: true});
    });
  }, []);
  return (
    <QueryClientProvider client={queryClient}>
      <ThemeProvider defaultTheme={defaultTheme}>
        <SafeAreaProvider>
          <NavigationContainer>
            <AppNavigator />
          </NavigationContainer>
        </SafeAreaProvider>
      </ThemeProvider>
    </QueryClientProvider>
  );
};

export default App;
