module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./'],
        alias: {
          '@modules': './modules/',
          '@emovie-ui': './modules/emovie-ui/',
        },
        extensions: [
          '.ios.ts',
          '.android.ts',
          '.ts',
          '.ios.tsx',
          '.android.tsx',
          '.tsx',
          '.jsx',
          '.js',
          '.json',
          '.native',
          '.native.js',
          '.native.json',
          '.native.ts',
          '.native.tsx',
        ],
      },
    ],
    [
      'babel-plugin-styled-components',
      {
        isNative: true,
      },
    ],
    ['@babel/plugin-proposal-decorators', {legacy: true}],
  ],
};
