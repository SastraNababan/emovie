module.exports = {
  root: true,
  extends: '@react-native-community',
  rules: {
    'react-native/no-inline-styles': 'off',
    'no-console': 'warn',
  },
  plugins: ['jest'],
  env: {
    'jest/globals': true,
  },
  ignorePatterns: ['/coverage/**/*.js'],
};
